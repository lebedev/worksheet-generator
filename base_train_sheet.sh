#!/bin/bash
# Sheet Generator to train base convertion
date="$(date +%Y%m%d%H%M%S).ms";
echo ".TS
allbox;
c c c c.
Dezimal 	Binärsystem 	Octalsystem 	Hexadezimal" >> $date
for i in {40..1};
	do
		case $((RANDOM %4+0)) in
			1)
				echo "	$(echo "obase=2;$((RANDOM/i))" | bc )" >> $date
				;;
			2)
				echo "		$(echo "obase=8;$((RANDOM/i))" | bc)" >> $date
				;;
			3)
				echo "			$(echo "obase=16;$((RANDOM/i))" | bc)" >> $date
				;;
			*)
				echo $((RANDOM/i)) >> $date
				;;
		esac
	done
echo ".TE"  >> $date

tbl "$date" | groff -k -e -ms -Tpdf > "${date/ms/pdf}"
rm $date
